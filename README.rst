Dotfiles
=========

This repository contains my lovely configuration files for various
unix-like operating systems. Commonly called 'dotfiles'.

Pretty much everything that I need to share among machines or with the world.
take whatever you find useful, if you fix an issue, by all means please send
your fixes back :)

May or may not contain an installation script.

Configurations Included
------------------------
- Stuff (TBD)

Stuff that *isn't* included
--------------------------------------
- vim config
  This may be found in its own repository_.
- Sensitive passwords, keys and such.
  Any file containing such things will either have been scrubbed or won't be commited until it can be scrubbed.
- Very, very generic things and most X specific configurations, unless amazing or painful to duplicate.


I know this sucks, but I will document eventually. Maybe.
If I don't forget.

.. _repository: https://github.com/mrdaemon/vimconfigs